#!/usr/bin/env bash

mkdir $HOME/.stat140
cd $HOME/.stat140
mkdir install
cd install

# Download Python3 Anaconda 2.3 Installer
echo ===DOWNLOADING ANACONDA===
wget https://repo.continuum.io/archive/Anaconda3-2.3.0-Linux-x86_64.sh

# Install Anaconda
echo ===INSTALLING ANACONDA===
bash Anaconda3-2.3.0-Linux-x86_64.sh -b -p $HOME/anaconda3
pathadd() {
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="$1:${PATH:+"$PATH"}"
    fi
}

# Add Anaconda to path temporarily
pathadd "$HOME/anaconda3/bin"

# Add the function above to the PATH permanently
cp $HOME/.bashrc $HOME/.bashrc-stat140.bak
echo "# Prepend to path without duplicates
pathadd() {
    if [ -d \"\$1\" ] && [[ \":\$PATH:\" != *\":\$1:\"* ]]; then
        PATH=\"\$1:\${PATH:+\"\$PATH\"}\"
    fi
}
" >> $HOME/.bashrc
echo "# Add Anaconda to path
pathadd \"\$HOME/anaconda3/bin\"" >> $HOME/.bashrc

# Update the conda package manager and install packages
# As of Sp16, PortAudio 19 is no longer used
echo ===INSTALLING CONDA PACKAGES===
conda install conda=3.15.1 conda-env=2.4.1 setuptools=18.1 ipython=3.2.1 ipython-notebook=3.2.1 jinja2=2.8 mistune=0.7 tornado=4.2.1 jsonschema=2.4.0 pyqt=4.10.4 matplotlib=1.4.3 numpy=1.9.2 scipy=0.16.0 datascience -y


echo ===VERIFY INSTALLATION===
cd ..
wget http://inst.eecs.berkeley.edu/~ee16a/sp16/install/check_version.py
chmod +x check_version.py
$HOME/anaconda3/bin/python3 check_version.py

echo ===INSTALLATION COMPLETE===
echo "If the 'conda' command is not recognized close and reopen your terminal or
run the command 'source ~/.bashrc'."
